import React, { Component } from 'react';

class Dashboard extends Component {
	logOut = () => {
		this.props.history.push('/');
	};

	render() {
		return (
			<React.Fragment>
				<div className="dashboard_page">
					<h1>This is Dashboard</h1>
					<button type="button" onClick={this.logOut} className="logout">
						Logout
					</button>
				</div>
			</React.Fragment>
		);
	}
}

export default Dashboard;

import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import Dashboard from './dashboard';

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			mobileNo: '9710582290',
			pin: 1000,
			errorMessage: '',
			active: false,
			pageRedirectFlag: false
		};
	}

	formValidate = (e) => {
		e.preventDefault();

		const mobileNo = document.getElementById('mobileNo').value;
		const pin = document.getElementById('pin').value;
		const confirmPin = document.getElementById('confirmPin').value;
		let indNum = /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/;

		this.timeOutMethod();

		// Empty Field Validation
		if (mobileNo == '') {
			{
				this.setState({ errorMessage: 'Mobile Number is required', active: true });
			}
			return;
		}
		// Mobile No : validation
		if (indNum.test(mobileNo)) {
			if (mobileNo === this.state.mobileNo) {
				{
					this.setState({ errorMessage: '' });
				}
			} else {
				{
					this.setState({ errorMessage: 'Invalid Login', active: true });
				}
				return;
			}
		} else {
			{
				this.setState({ errorMessage: 'Enter a valid mobile number', active: true });
			}
			return;
		}

		if (pin == '') {
			{
				this.setState({ errorMessage: 'PIN field is required', active: true });
			}
			return;
		}
		if (pin.length !== 4) {
			{
				this.setState({ errorMessage: '4 Digit PIN is required', active: true });
			}
			return;
		}
		if (confirmPin == '') {
			{
				this.setState({ errorMessage: 'ConfirmPin field is required', active: true });
			}
			return;
		}
		if (confirmPin.length !== 4) {
			{
				this.setState({ errorMessage: '4 Digit PIN is required for Confirm PIN', active: true });
			}
			return;
		}
		// PIN code : validation
		if (pin === confirmPin) {
			if (pin == this.state.pin) {
				{
					this.setState({ errorMessage: '' });
					this.props.history.push('/dashboard');
				}
			} else {
				{
					this.setState({ errorMessage: 'Invalid Login', active: true });
				}
				return;
			}
		} else {
			{
				this.setState({ errorMessage: 'PIN and Confirm PIN is mistaching', active: true });
			}
			return;
		}
	};

	timeOutMethod = () => {
		setTimeout(() => {
			{
				this.setState({
					active: false
				});
			}
		}, 500);
	};

	render() {
		return (
			<React.Fragment>
				<div className="body">
					<div className="container">
						<div className="register_body">
							<h1>LOGIN FORM</h1>
							<form name="login_form">
								<div className="formm-group">
									<h5>MOBILE NUMBER</h5>
									<input
										type="text"
										className="formm-control"
										id="mobileNo"
										placeholder="MOBILE NUMBER"
									/>
								</div>
								<div className="formm-group">
									<h5>PIN</h5>
									<input type="password" className="formm-control" id="pin" placeholder="PIN" />
								</div>
								<div className="formm-group">
									<h5>CONFIRM PIN</h5>
									<input
										type="password"
										className="formm-control"
										id="confirmPin"
										placeholder="CONFIRM PIN"
									/>
								</div>
								<div className="formm-group">
									<input
										type="submit"
										onClick={this.formValidate}
										className="formm-control"
										id="form_submit"
										value="LOGIN"
									/>
								</div>
							</form>
							<div className="error_message">
								<h5 className={this.state.active == true ? 'active' : 'inactive'}>
									{this.state.errorMessage}
								</h5>
							</div>
						</div>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default Login;

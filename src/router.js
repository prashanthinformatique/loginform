import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import Login from './login';
import Dashboard from './dashboard';
import './app.scss';

class RouterBlock extends Component {
	render() {
		return (
			<React.Fragment>
				<Router>
					<Route exact path="/" component={Login} />
					<Route path="/dashboard" component={Dashboard} />
					<Route render={() => <Redirect to={{ pathname: '/' }} />} />
				</Router>
			</React.Fragment>
		);
	}
}

export default RouterBlock;
